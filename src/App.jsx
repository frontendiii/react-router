import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Navbar from './components/Navbar'
import Beer from './Beer'
import Home from './Home'
import Contacto from './Contacto'

function App() {
  return (
   <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/home" element={<Home />} />
        <Route path="/contacto" element={<Contacto />} />
        <Route path="/beer/:id" element={<Beer />} />
      </Routes> 
   </BrowserRouter>
  )
}

export default App
